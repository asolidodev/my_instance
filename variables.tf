variable "my_instance_create" {
  type        = string
  description = "Whether to create the instance(s) and the associated resources."
}
variable "my_instance_name_prefix" {
  type        = string
  description = "A string of names to add to the instance name and hostname to identify the organization, environment, sub-environment, etc. For example, 'bigclient-production-us'. If the terraform workspace is not 'default', the workspace gets appended to this prefix. For example for a 'test' workspace you get 'bigclient-production-us-test-'"
}
variable "my_instance_name" {
  type        = string
  description = "The name used in the instance name (and hostname). For example, 'gw' for a gateway or 'lb' for a load balancer. The my_instance_name_prefix is prepended (including any non-default workspace name). A number is appended."
}
variable "my_key_name_prefix" {
  type        = string
  description = "A prefix that gets prepended to the key name for the public SSH key. It should include any needed separator in the end. Example 'bigclient_production_us_' (when workspaces are not used in practise) or 'bigclient_production_us_default_' when workspaces are used in practise."
}
variable "my_key_name_default" {
  type        = string
  description = "The key name that is used for the instance whenever it is not specified for the instance. In both cases, the corresponding prefix is added (i.e. my_key_name_prefix)."
}
variable "my_instance_domain_name" {
  type        = string
  description = "The domain_name to be used, e.g. example.com, that gets appended to the hostname (dot separated)."
}
variable "my_instance_configs" {
  # FIXME as a temporary workaround we use any for this list of maps of...
  type        = any
  description = "A list (of maps) with the configurations for the instances. The lenght of this list is used to determine how many instances to launch"
}
variable "my_instance_vpc_id" {
  type        = string
  description = "The VPC id where to place the instance(s)"
}
variable "my_instance_ami_default" {
  type        = string
  description = "The AMI used for the instance instances whenever an AMI is not specified for the instance"
}
variable "my_instance_type_default" {
  type        = string
  description = "The instance type used for the instance whenever an instance type is not specified for the instance"
}
variable "my_instance_ebs_optimized_default" {
  type        = bool
  description = "If the instance is ebs optimized whenever ebs_optimized is not specified for the instance"
  default     = false
}
variable "my_instance_az_id" {
  type        = list(any)
  description = "The list of AZs where the instances are placed (this list is wrapped around if needed)"
}
variable "my_instance_subnet_id" {
  type        = list(any)
  description = "The list of subnets where the instances are be placed (this list is wrapped around if needed)"
}
variable "my_instance_root_volume_size_gb_default" {
  type        = string
  description = "The root volume size (in GB) that is used for the instance instance whenever such size is not specified for the instance"
}
variable "my_instance_security_groups" {
  type        = list(string)
  description = "The list of security groups to which the instance will belong."
}
variable "my_instance_associate_public_ip" {
  type        = string
  default     = "false"
  description = "Whether to associate a public ip with the instances. False by default because we expect most instances to be in private subnets so that would be the most frequent case."
}
variable "my_instance_tag" {
  type        = map(any)
  description = "The tag(s) used in the instances. Unfortunately passing custom tags per instance is not so easy so these will be the same in all instances. These tags are in addition to the ones the module adds (Name, ManagedByTerraform, and TerraformWorkspace)."
}
variable "my_instance_puppet_master_fqdn" {
  type        = string
  description = "The fqdn of the instance's puppet master. If an empty string is used here (the default) then puppet.domain_name is used."
  default     = ""
}
variable "my_instance_puppet_master_ip" {
  type        = string
  description = "The ip of the instance's puppet master."
}
variable "my_instance_puppet_environment" {
  type        = string
  description = "The puppet environment (e.g. 'production' or 'staging'). We need this for the puppet node configuration."
}
variable "my_instance_template_file_install_puppet_node" {
  type        = string
  description = "A local filepath to use for the template to install puppet on instance launch (instead of the module's default template)"
  default     = ""
}
variable "my_instance_iam_instance_profile_default" {
  type        = string
  description = "IAM instance profile to use for the instances. For now this is actually more than a default: it is always used. Calling it default allows us to easily extend this module in the future to support per-instance iam_instance_profile if needed, without breaking backwards compatibility and consistency."
  default     = null
}
