# my_instance

Terraform module used to create AWS instances

## Table of Contents

- [my_instance](#my_instance)
  - [Table of Contents](#table-of-contents)
  - [Example](#example)
  - [Requirements](#requirements)
  - [Variables](#variables)
  - [Considerations](#considerations)

## Example

Below is a small example.

```hcl
module "my_instance_webserver" {
  source                  = "git::ssh://git@bitbucket.org/asolidodev/my_instance.git?ref=3.0.3"
  my_instance_create      = var.webserver_create ? 1 : 0
  my_instance_name_prefix = local.instance_name_prefix
  my_instance_name        = var.webserver_name

  # Notice we add the separator
  my_key_name_prefix      = "${local.resource_name_prefix}_"
  my_key_name_default     = var.key_name_default
  my_instance_domain_name = var.main_dns_zone
  my_instance_configs     = var.webserver_configs

  # We get the VPC ID from the module (i.e. it's NOT an input variable)
  my_instance_vpc_id       = data.aws_vpc.my_vpc.id
  my_instance_ami_default  = var.ubuntu_ami
  my_instance_type_default = var.webserver_instance_type_default

  # Let's use all the AZs from the VPC
  my_instance_az_id                       = var.availability_zones_vpc
  my_instance_subnet_id                   = data.aws_subnet.private.*.id
  my_instance_root_volume_size_gb_default = var.webserver_root_volume_size_gb_default

  my_instance_security_groups = var.create_sg ? local.webserver_sg_list : local.default_sg_list

  my_instance_associate_public_ip = var.webserver_associate_public_ip
  my_instance_tag                 = var.webserver_tag
  my_instance_puppet_master_fqdn  = var.puppet_master_fqdn
  my_instance_puppet_master_ip    = var.puppet_master_ip
  my_instance_puppet_environment  = var.puppet_environment

  my_instance_iam_instance_profile_default = var.lambda_invoke_permission ? aws_iam_instance_profile.webserver_profile[0].name : null
}
```

## Requirements

|   Name    | Version   |
| --------- | --------- |
| terraform | >= 0.13.0 |
|   aws     | >= 3.0.0  |

## Variables

| Name | Description | Type | Required | Default |
|:---:|:---:|:---:|:---:|:---:|
| `my_instance_create` | Whether to create the instance(s) and the associated resources. | string | yes | - |
| `my_instance_name_prefix` | A string of names to add to the instance name and hostname to identify the organization, environment, sub-environment, etc. For example, 'bigclient-production-us'. If the terraform workspace is not 'default', the workspace gets appended to this prefix. For example for a 'test' workspace you get 'bigclient-production-us-test-' | string | yes | - |
| `my_instance_name` | The name used in the instance name (and hostname). For example, 'gw' for a gateway or 'lb' for a load balancer. The my_instance_name_prefix is prepended (including any non-default workspace name). A number is appended. | string | yes | - |
| `my_key_name_prefix` | A prefix that gets prepended to the key name for the public SSH key. It should include any needed separator in the end. Example 'bigclient_production_us_' (when workspaces are not used in practise) or 'bigclient_production_us_default_' when workspaces are used in practise. | string | yes | - |
| `my_key_name_default` | The key name that is used for the instance whenever it is not specified for the instance. In both cases, the corresponding prefix is added (i.e. my_key_name_prefix). | string | yes | - |
| `my_instance_domain_name` | The domain_name to be used, e.g. example.com, that gets appended to the hostname (dot separated). | string | yes | - |
| `my_instance_configs` | A list (of maps) with the configurations for the instances. The lenght of this list is used to determine how many instances to launch | any | yes | - |
| `my_instance_vpc_id` | The VPC id where to place the instance(s) | string | yes | - |
| `my_instance_ami_default` | The AMI used for the instance instances whenever an AMI is not specified for the instance | string | yes | - |
| `my_instance_type_default` | The instance type used for the instance whenever an instance type is not specified for the instance | string | yes | - |
| `my_instance_ebs_optimized_default` | If the instance is ebs optimized whenever ebs_optimized is not specified for the instance | bool | no | `false` |
| `my_instance_az_id` | The list of AZs where the instances are placed (this list is wrapped around if needed) | list(any) | yes | - |
| `my_instance_subnet_id` | The list of subnets where the instances are be placed (this list is wrapped around if needed) | list(any) | yes | - |
| `my_instance_root_volume_size_gb_default` | The root volume size (in GB) that is used for the instance instance whenever such size is not specified for the instance | string | yes | - |
| `my_instance_security_groups` | The list of security groups to which the instance will belong. | list(string) | yes | - |
| `my_instance_associate_public_ip` | Whether to associate a public ip with the instances. False by default because we expect most instances to be in private subnets so that would be the most frequent case. | string | no | `"false"` |
| `my_instance_tag` | The tag(s) used in the instances. Unfortunately passing custom tags per instance is not so easy so these will be the same in all instances. These tags are in addition to the ones the module adds (Name, ManagedByTerraform, and TerraformWorkspace). | map(any) | yes | - |
| `my_instance_puppet_master_fqdn` | The fqdn of the instance's puppet master. If an empty string is used here (the default) then puppet.domain_name is used. | string | no | `""` |
| `my_instance_puppet_master_ip` | The ip of the instance's puppet master. | string | yes | - |
| `my_instance_puppet_environment` | The puppet environment (e.g. 'production' or 'staging'). We need this for the puppet node configuration. | string | yes | - |
| `my_instance_template_file_install_puppet_node` | A local filepath to use for the template to install puppet on instance launch (instead of the module's default template) | string | no | `""` |
| `my_instance_iam_instance_profile_default` | IAM instance profile to use for the instances. For now this is actually more than a default: it is always used. Calling it default allows us to easily extend this module in the future to support per-instance iam_instance_profile if needed, without breaking backwards compatibility and consistency. | string | no | `null` |

## Considerations

- The **number of instances** is equal to the length of the list `my_instance_configs`