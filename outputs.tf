output "my_prefix_naming" {
  value = local.my_prefix_naming
}
output "my_instance_public_ip" {
  value = aws_instance.my_instance.*.public_ip
}
output "my_instance_private_ip" {
  value = aws_instance.my_instance.*.private_ip
}
output "my_instance_id" {
  value = aws_instance.my_instance.*.id
}
