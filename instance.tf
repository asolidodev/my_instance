resource "aws_instance" "my_instance" {
  count                  = var.my_instance_create ? length(var.my_instance_configs) : 0
  ami                    = lookup(var.my_instance_configs[count.index], "ami", var.my_instance_ami_default)
  instance_type          = lookup(var.my_instance_configs[count.index], "instance_type", var.my_instance_type_default)
  vpc_security_group_ids = var.my_instance_security_groups
  availability_zone      = element(var.my_instance_az_id, count.index)
  subnet_id              = element(var.my_instance_subnet_id, count.index)
  key_name               = "${var.my_key_name_prefix}${lookup(var.my_instance_configs[count.index], "key_name", var.my_key_name_default)}"
  user_data              = data.template_file.my_instance_install_puppet_node.*.rendered[count.index]
  iam_instance_profile   = var.my_instance_iam_instance_profile_default
  ebs_optimized          = lookup(var.my_instance_configs[count.index], "ebs_optimized", var.my_instance_ebs_optimized_default)

  associate_public_ip_address = lookup(var.my_instance_configs[count.index], "associate_public_ip_address", var.my_instance_associate_public_ip)

  root_block_device {
    volume_type = "gp2"
    # size is in GiBs
    volume_size = lookup(var.my_instance_configs[count.index], "root_volume_size_gb", var.my_instance_root_volume_size_gb_default)
    # explicit the default delete on termination
    delete_on_termination = true
  }

  # For workarounds to tagging with vars and merges see here
  # (for now we're following the terraform < 12 workaround):
  # https://github.com/hashicorp/terraform/issues/14516
  tags = merge(
    tomap({
      "ManagedByTerraform" = "true",
      "TerraformWorkspace" = terraform.workspace,
      "Name"               = format("%s-%02d", local.my_prefix_naming, count.index + 1),
    }),
    var.my_instance_tag,
  )

  # We don't want instance recreation
  lifecycle {
    ignore_changes = [user_data]
  }

}

data "template_file" "my_instance_install_puppet_node" {
  count = var.my_instance_create ? length(var.my_instance_configs) : 0
  # Workaround that a variable default cannot have interpolation
  template = file(lookup(var.my_instance_configs[count.index], "user_data", var.my_instance_template_file_install_puppet_node) == "" ? "${path.module}/install-puppet-node.sh.tpl" : lookup(var.my_instance_configs[count.index], "user_data", var.my_instance_template_file_install_puppet_node))
  vars = {
    master_fqdn = local.puppet_master_fqdn_parsed
    master_ip   = var.my_instance_puppet_master_ip
    nodename    = format("%s-%02d", local.my_prefix_naming, count.index + 1)
    domain      = var.my_instance_domain_name
    environment = var.my_instance_puppet_environment
  }
}
